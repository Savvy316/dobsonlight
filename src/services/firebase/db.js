import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";
import { Notify } from "quasar";

/**
 * Firestore
 * https: //firebase.google.com/docs/reference/js/firebase.firestore.Firestore.html
 *
 * @return {Interface} returns Firestore
 */
export const firestore = () => {
  return firebase.firestore();
};

/**
 * @param  {String} collectionName - Firestore collection name
 * @param  {String} id - Uid to assign to a doc in firestore collection
 */
export const productRef = (collectionName, id) => {
  return firestore()
    .collection(collectionName)
    .doc(id);
};

/**
 *
 * @param {String} collectionName - The name of the collecte the object will belong to
 * @param {Object} object - The attribute to be created
 */
export const create = (collectionName, object) => {
  const { cover, ...obj } = object;
  var newObj = {
    createdAt: Date.now(),
    updatedAt: Date.now(),
    ...obj
  };
  // console.log(newObj)
  return firestore()
    .collection(collectionName)
    .add(newObj);
};

export const find = (collectionName, query) => {
  return firestore()
    .collection(collectionName)
    .where("category", "==", query)
    .get()
    .then(snapshot => {
      var products = [];
      snapshot.docs.forEach(doc => {
        let items = doc.data();
        /* Make data suitable for rendering */
        // items = JSON.stringify(items);
        /* Update the components state with query result */
        // this.setState({ items: items });
        products.push({ id: doc.id, ...items });
      });
      return products;
    })
    .catch(error => {
      Notify.create({
        message: `${error.message}`,
        color: "negative"
      });
    });
};

/**
 *
 * @param {Object} file - The file object to be uploaded
 * @param {String} title - The title of the file
 */
export const coverImageUpload = title => {
  return firebase.storage().ref(`covers/${title}`);
};
// export const coverImageUpload = (file, title) => {
//   return new Promise((resolve, reject) => {
//     try {
//       firebase
//         .storage()
//         .ref(`covers/${title}`)
//         .put(file)

//         .then(
//           snapshot => {
//             snapshot.ref.getDownloadURL().then(url => {
//               console.log("url", url);
//             });
//             resolve(snapshot);
//           }
//           // e => console.log(e) //<-- maybe we can get more information?
//         )
//         .catch(e => {
//           reject(e);
//         });
//     } catch (error) {
//       console.warn("What the heck?? ", error);
//     }
//   });
// };

export const updateDocument = (collectionName, documentId, editedObject) => {
  return firestore()
    .collection(collectionName)
    .doc(documentId)
    .update(editedObject)
    .then(function(data) {
      console.error("update successfull ", data);

      return { id: documentId, edited: true };
    })
    .catch(function(error) {
      console.error("Error editing document: ", error);
    });
};

// export const deleteDocument = (collectionName, documentId) => {
//   return firestore()
//     .collection(collectionName)
//     .doc(documentId)
//     .delete()
//     .then(function() {
//       return { id: documentId, deleted: true };
//     })
//     .catch(function(error) {
//       console.error("Error removing document: ", error);
//     });
// };

/**
 * @param  {String} storageLocation - Location on Firebase Storage
 */
export const storageRef = storageLocation => {
  return firebase.storage().ref(storageLocation);
};
