const routes = [
  {
    path: "/",
    redirect: "/home",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "/home", component: () => import("pages/Index.vue") },
      {
        path: "/doby/add",
        component: () => import("pages/Doby/New-Prod.vue"),
        meta: {
          requiresAuth: true
        }
      }
    ]
  },
  {
    path: "/products/:category",
    component: () => import("layouts/ProductCategoryLayout.vue"),
    children: [
      {
        path: "",
        name: "products",
        // path: "/products/:category",
        component: () => import("pages/Categories/_products.vue")
      }
    ]
  },
  {
    path: "/auth",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "login",
        name: "Login",
        component: () => import("pages/Auth.vue")
      },
      // {
      //   path: "register",
      //   name: "Register",
      //   component: () => import("pages/Auth.vue")
      // }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
