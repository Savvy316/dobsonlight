export const currentUser = ({ currentUser }) => currentUser;
export const editUserDialog = ({ editUserDialog }) => editUserDialog;
export const findProducts = ({ categories }, getters) => {
  return categories[getters] || [];
};
