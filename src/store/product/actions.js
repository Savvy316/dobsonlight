import { firestoreAction } from "vuexfire";
import { Notify } from "quasar";

import {
  productRef,
  create,
  find,
  updateDocument,
  deleteDocument,
  coverImageUpload
} from "../../services/firebase/db.js";

/** Get current user from the firestore collection user's
 * via firebase uid
 *
 * @param  {Ojbect} payload.id - Firebase currentUser id
 */
export const getCurrentProduct = firestoreAction(({ bindFirestoreRef }, id) => {
  return bindFirestoreRef("currentProduct", productRef("product", id));
});

/**
 * @param  {Object} {state}
 * @param  {String} id
 * @param  {Object} payload
 */
export const editProduct = function({ commit }, payload) {
  var { id, cover, ...load } = payload;
  var obj = { ...load };
  return productRef("products", payload.id)
    .update(obj)
    .then(() => {
      commit("setEditedProduct", payload);
      Notify.create({
        message: "Product Updated Successfully",
        color: "green"
      });
    })
    .catch(error => {
      Notify.create({
        message: "An Error Occured while trying to Update Product",
        color: "red"
      });
    });
};

/**
 *
 * @param {String} payload - The category name
 */
export const getProductsInCategory = async function(
  { state, commit },
  payload
) {

  return find("products", payload).then(products => {
    commit("setProducts", { category: payload, products });
  });
};

/**
 *
 * @param {Object} payload - The object to be created
 */
export const createProduct = async function({ state }, payload) {
  // return create("products", payload).then(data => {
  //   var upload = coverImageUpload(payload.cover, payload.title).then(path => {
  //     if (path) {
  //       return path;
  //     }
  //   });
  //   return upload;
  // });
  return coverImageUpload(payload.title)
    .put(payload.cover)
    .then(snapshot => {
      snapshot.ref.getDownloadURL().then(url => {
        create("products", { image: url, ...payload }).then(data => {
          Notify.create({
            message: "Product Created Successfully",
            color: "green"
          });
        });
      });
    })
    .catch(error => {
      Notify.create({
        message: "An Error Occured while trying to create Product",
        color: "red"
      });
    });
};

export const deleteProduct = async function({ commit }, payload) {
  // deleteDocument("products", payload.id).then(data => {
  //   console.log(data);
  //   if (data.deleted == true) {
  //     commit("removeProduct", payload);
  //   }
  // });
  return productRef("products", payload.id)
    .delete()
    .then(() => {
      commit("setEditedProduct", payload);
      Notify.create({
        message: "Product Deleted Successfully",
        color: "green"
      });
    })
    .catch(error => {
      Notify.create({
        message: "An Error Occured while trying to Delete Product",
        color: "red"
      });
    });
};
