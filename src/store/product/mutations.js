import product from ".";

export const setCurrentUserData = (state, data) => {
  state.currentUser = data;
};

export const setEditUserDialog = (state, editUserDialog) => {
  state.editUserDialog = editUserDialog;
};

export const setProducts = (state, data) => {
  // var ty = { [data[0].category]: data };
  // if (data[0]) {
  //   state.categories[data[0].category] = data;
  // }
  console.log("mutation :", data);

  if (data.category) {
    state.categories[data.category] = data.products;
  } else {
    state.categories["Osram"] = data.posts;
  }
};

export const removeProduct = (state, product) => {
  var reSet = state.categories[product.category].filter((val, ind) => {
    return val.id != product.id;
  });
  state.categories[product.category] = reSet;
};

export const setEditedProduct = (state, product) => {
  var productIndex = state.categories[product.category].findIndex(
    (val, index) => {
      return val.id == product.id;
    }
  );
  state.categories[product.category][productIndex] = product;
};
